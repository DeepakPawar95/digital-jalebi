module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        uglify: {
            options: {
                sourceMap: true,
                mangle: false,
                compress: {
                    drop_console: true
                },
                preserveComments: false
            },
            build: {
                files: {
                    'build/js/angular.min.js': [
                        'app/bower_components/angular/angular.js',
                        'app/bower_components/angular-route/angular-route.js',
                        'app/components/version/version.js',
                        'app/components/version/version-directive.js',
                        'app/components/version/interpolate-filter.js'
                    ],
                    'build/js/all.min.js': [
                        'app/js/app.js',
                        'app/js/routers/*.js',
                        'app/js/directives.js',
                        'app/js/controllers/*.js'
                    ],
                    'build/js/jquery_masonry.min.js': [
                        'app/bower_components/jquery/dist/jquery.min.js',
                        'app/bower_components/masonry/dist/masonry.pkgd.min.js'
                    ]
                }
            }
        },
        cssmin: {
            combine: {
                options: {
                    sourceMap: true,
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                files: {
                    'build/styles/css/all.css': [
                        'app/app.css',
                        'app/styles/css/header.css',
                        'app/styles/css/footer.css',
                        'app/styles/css/index.css',
                        'app/styles/css/clients.css'
                    ]
                }
            },
            minify: {
                expand: true,
                cwd: 'build/styles/css/',
                src: ['*.css', '!*.min.css'],
                dest: 'build/styles/css/',
                ext: '.min.css'
            }
        },
        htmlmin: {                                     // Task
            dist: {                                      // Target
                options: {                                 // Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [
                    {
                        expand: true,
                        cwd: 'app/views',
                        src: '**/*.html',
                        dest: 'build/views'
                    }
                ]
            }
        },
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: 'app/img',
                        src: '**',
                        dest: 'build/img'
                    },
                    {
                        expand: true,
                        cwd: 'app/styles/css',
                        src: ['about.css', 'connect.css', 'project.css'],
                        dest: 'build/styles/css'
                    }
                ]
            }
        }
    });


    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Default task(s).
    grunt.registerTask('default', ['uglify', 'cssmin', 'htmlmin', 'copy']);

};