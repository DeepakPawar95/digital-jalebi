
app.controller('footerController', ['$scope', function($scope) {
    angular.element(document).ready(function () {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 300) {
                showElements('.site-share');
                showElements('.site-actions-up');
            } else {
                hideElements('.site-share');
                hideElements('.site-actions-up');
            }
        });

        function showElements(element) {
            $(element).each(function(i, el) {
                setTimeout(function() {
                    $(el).addClass('active');
                }, i * 100);
            });
        }

        function hideElements(element) {
            $(element).each(function(i, el) {
                setTimeout(function() {
                    $(el).removeClass('active');
                }, i * 100);
            });
        }
    });

    $scope.scrollToTop = function () {
        $('html,body').animate({
            scrollTop: $('html').offset().top
        }, 1000);
    }

}]);