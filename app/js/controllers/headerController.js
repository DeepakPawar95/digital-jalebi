app.controller('headerController', ['$scope', '$route', '$timeout', function ($scope, $route, $timeout) {
    $scope.$route = $route;

    $scope.showLoading = true;
    $scope.mobileNav = false;

    $timeout(function () {
        $scope.showLoading = false;
    }, 1000);

    $(document).ready(function() {
        setTimeout(function(){
            $('.loading-container').fadeOut().remove();
        }, 1000);

    });

    $scope.showMenu = function () {
        $scope.mobileNav = true;
    };

    $scope.hideMenu = function () {
        $scope.mobileNav = false;
    };

}]);