app.controller('homeController', ['$interval', function ($interval) {

    $interval(function () {
        $('.masonry-grid').masonry({
            // options
            itemSelector: '.masonry-grid-item',
            columnWidth: 10,
            gutter: 3
        });
    }, 2000);

    $(document).ready(function () {

        setTimeout(function () {
            //setMasonry();
            $('.masonry-grid').masonry({
                // options
                itemSelector: '.masonry-grid-item',
                columnWidth: 10,
                gutter: 3
            });

            //$('.loading-container').fadeOut().remove();
        }, 1000);

    });


    angular.element(document).ready(function () {

        setTimeout(function () {
            //setMasonry();
        }, 100);


        /* Projects filter */
        $('.category li a').click(function (e) {
            e.preventDefault();

            $('.category li a').removeClass('active-tab');
            $(this).addClass('active-tab');

            var category = $(this).attr('data-type').toLowerCase().split("&");
            if (category[0] === "all") {
                $('.masonry-grid-item').show();
                setMasonry();
            }
            else {
                //hide all categories
                $('.masonry-grid-item').hide();
                $.each(category, function (i, v) {
                    $('.masonry-grid-item.' + v.trim()).show();
                    setMasonry();
                });
            }
        });


        /* Arrange cards using masonry */
        function setMasonry() {
            $('.masonry-grid').masonry({
                // options
                itemSelector: '.masonry-grid-item',
                columnWidth: 10,
                gutter: 3
            });
        }


        /* Set tooltip position */
        var tooltips = document.querySelectorAll('.entry-caption');

        window.onmousemove = function (e) {
            var x = (e.clientX + 20) + 'px',
                y = (e.clientY + 20) + 'px';
            for (var i = 0; i < tooltips.length; i++) {
                tooltips[i].style.top = y;
                tooltips[i].style.left = x;
            }
        };


    });

}]);