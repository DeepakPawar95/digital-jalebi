app.controller('projectsController', ['$window', function ($window) {

    $window.scrollTo(0, 0);

    angular.element(document).ready(function () {

        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll >= 100) {
                $('.hero-header').addClass('active');
            } else {
                $('.hero-header').removeClass('active');
            }
        });

    });

}]);