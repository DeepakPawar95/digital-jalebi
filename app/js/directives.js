/**
 * Created by deepak on 22/09/17.
 */

/* Directives */

app.directive('headerCustom', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/header.html',
        controller: 'headerController'
    };
});

app.directive('footerCustom', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/footer.html',
        controller: 'footerController'
    };
});

app.directive('projectsList', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/projects-list.html',
        controller: 'homeController'
    };
});
