/**
 * Created by deepak on 22/09/17.
 */

/* Routers */

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $routeProvider
        .when('/media/', {
            templateUrl: 'views/media.html',
            activetab: 'media'
        })
        .when('/media/bangalore-mirror/', {
            templateUrl: 'views/media/bangalore-mirror.html'
        })
        .when('/media/designwala/', {
            templateUrl: 'views/media/designwala.html'
        })
        .when('/media/dna/', {
            templateUrl: 'views/media/dna.html'
        })
        .when('/media/ht-city/', {
            templateUrl: 'views/media/ht-city.html'
        })
        .when('/media/caravan/', {
            templateUrl: 'views/media/caravan.html'
        })
        .when('/media/hindu/', {
            templateUrl: 'views/media/hindu.html'
        })
        .otherwise({redirectTo: '/'});
}]);
