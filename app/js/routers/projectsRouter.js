

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $routeProvider
        .when('/projects/bosch/', {
            templateUrl: 'views/projects/bosch.html',
            controller: 'projectsController'
        })
        .when('/projects/ngcm2m/', {
            templateUrl: 'views/projects/ngcm2m.html',
            controller: 'projectsController'
        })
        .when('/projects/soccerfun/', {
            templateUrl: 'views/projects/soccerFun.html',
            controller: 'projectsController'
        })
        .when('/projects/shauryaSmarak/', {
            templateUrl: 'views/projects/shauryaSmarak.html',
            controller: 'projectsController'
        })
        .when('/projects/bmgf/', {
            templateUrl: 'views/projects/bmgf.html',
            controller: 'projectsController'
        })
        .when('/projects/siyaKeRam/', {
            templateUrl: 'views/projects/siyaKeRam.html',
            controller: 'projectsController'
        })
        .when('/projects/tata/', {
            templateUrl: 'views/projects/tata.html',
            controller: 'projectsController'
        })
        .when('/projects/jabongRampWalk/', {
            templateUrl: 'views/projects/jabongRampWalk.html',
            controller: 'projectsController'
        })
        .when('/projects/tintedWater/', {
            templateUrl: 'views/projects/tintedWater.html',
            controller: 'projectsController'
        })
        .when('/projects/parkAvenueAW/', {
            templateUrl: 'views/projects/parkAvenueAW.html',
            controller: 'projectsController'
        })
        .when('/projects/colorNextAP/', {
            templateUrl: 'views/projects/colorNextAP.html',
            controller: 'projectsController'
        })
        .when('/projects/roposo/', {
            templateUrl: 'views/projects/roposo.html',
            controller: 'projectsController'
        })
        .when('/projects/toyotaAutoExpo/', {
            templateUrl: 'views/projects/toyotaAutoExpo.html',
            controller: 'projectsController'
        })
        .when('/projects/jaltarang/', {
            templateUrl: 'views/projects/jaltarang.html',
            controller: 'projectsController'
        })
        .when('/projects/kathputliColony/', {
            templateUrl: 'views/projects/kathputliColony.html',
            controller: 'projectsController'
        })
        .when('/projects/cokeArDiljit/', {
            templateUrl: 'views/projects/cokeArDiljit.html',
            controller: 'projectsController'
        })
        .when('/projects/doritosVertigoChallenge/', {
            templateUrl: 'views/projects/dvc.html',
            controller: 'projectsController'
        })
        .when('/projects/geetaMahautsav/', {
            templateUrl: 'views/projects/geetaMahautsav.html',
            controller: 'projectsController'
        })
        .when('/projects/glenmorangieInteractiveLaunch/', {
            templateUrl: 'views/projects/gil.html',
            controller: 'projectsController'
        })
        .when('/projects/google/', {
            templateUrl: 'views/projects/google.html',
            controller: 'projectsController'
        })
        .when('/projects/jaquar/', {
            templateUrl: 'views/projects/jaquar.html',
            controller: 'projectsController'
        })
        .when('/projects/wallInteractiveProjection/', {
            templateUrl: 'views/projects/wallIP.html',
            controller: 'projectsController'
        })
        .when('/projects/acmaCarSimulation/', {
            templateUrl: 'views/projects/acmaCarSimulation.html',
            controller: 'projectsController'
        })
        .when('/projects/varanasiMuseum/', {
            templateUrl: 'views/projects/varanasiMuseum.html',
            controller: 'projectsController'
        })
        .otherwise({redirectTo: '/'});
}]);
