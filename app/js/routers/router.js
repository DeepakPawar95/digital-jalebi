/**
 * Created by deepak on 22/09/17.
 */

/* Routers */

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            activetab: 'home'
        })
        .when('/about/', {
            templateUrl: 'views/about.html',
            controller: 'projectsController',
            activetab: 'about'
        })
        .when('/clients/', {
            templateUrl: 'views/clients.html',
            activetab: 'clients'
        })
        .when('/connect/', {
            templateUrl: 'views/connect.html',
            controller: 'projectsController',
            activetab: 'connect'
        })
        .otherwise({redirectTo: '/'});
}]);
